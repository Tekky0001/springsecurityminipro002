insert into users ( username, password, full_name, image_url)
values ('andy', '123', 'andy','no-img.png');
insert into users ( username, password, full_name, image_url)
values ( 'kelly', '123', 'kelly','no-img.png');

insert into posts( caption, image, user_id)
values ( 'first post', 'no-img.png',1);
insert into posts( caption, image, user_id)
values ( 'second post', 'no-img.png',1);
insert into posts( caption, image, user_id)
values ( 'third post', 'no-img.png',2);

insert into comments ( content, parent_id, post_id, user_id)
values ('first comment', 0, 1,1);
insert into comments ( content, parent_id, post_id, user_id)
values ( 'first_first comment', 1, 1,2);
insert into comments ( content, parent_id, post_id, user_id)
values ('another comment', 0, 2,2);

insert into user_like( post_id, user_id)
values ( 1, 1);
insert into user_like( post_id, user_id)
values ( 1,2);

insert into roles( role_status)
values( 'ADMIN');
insert into roles( role_status)
values( 'USER');

insert into user_role(user_id, role_id)
values(1,1);
insert into user_role(user_id, role_id)
values(1,2);
insert into user_role(user_id, role_id)
values(2,2);


