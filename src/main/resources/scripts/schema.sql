drop table if exists users cascade ;
drop table if exists posts cascade ;
drop table if exists comments cascade ;
drop table if exists user_like cascade ;
drop table if exists roles cascade ;
drop table if exists user_role cascade ;

create table if not exists  users(
    id serial primary key not null,
    username text not null,
    password text not null,
    full_name text not null,
    image_url text not null
);
create table if not exists posts(
    id serial primary key not null,
    caption text not null,
    image text not null,
    number_of_like int4,
    user_id int4 not null references users(id) on delete cascade
);
create table if not exists comments(
    id serial primary key not null,
    content text not null,
    parent_id int4 not null,
    post_id int4 not null references posts(id) on delete cascade ,
    user_id int4 not null references users(id) on delete cascade
);

create table if not exists user_like(
    id serial primary key not null,
    post_id int4 not null references posts(id) on delete cascade,
    user_id int4 not null references users(id) on delete cascade
);
create table if not exists roles(
    id serial primary key not null,
    role_status text not null
);
create table if not exists user_role(
    user_id int4  references users(id),
    role_id int4  references roles(id),
    primary key (user_id, role_id)
);

