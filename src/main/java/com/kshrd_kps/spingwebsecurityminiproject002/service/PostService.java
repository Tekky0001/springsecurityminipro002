package com.kshrd_kps.spingwebsecurityminiproject002.service;

import com.kshrd_kps.spingwebsecurityminiproject002.model.Post;
import com.kshrd_kps.spingwebsecurityminiproject002.model.PostDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.PostRequest;

import java.util.List;

public interface PostService {

    List<PostDto> getAllPosts(Integer page, Integer size);

    PostDto getPostByPostId(Integer post_id);

    void createPost(PostRequest postRequest);


    PostDto getNewInsertedPost();

    void updatePost(Integer id, PostRequest postRequest);

    void deletePost(Integer id);
}
