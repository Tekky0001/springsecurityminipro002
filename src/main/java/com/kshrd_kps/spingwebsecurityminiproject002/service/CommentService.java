package com.kshrd_kps.spingwebsecurityminiproject002.service;

import com.kshrd_kps.spingwebsecurityminiproject002.model.Comment;
import com.kshrd_kps.spingwebsecurityminiproject002.model.CommentDto;

public interface CommentService {
    public CommentDto getCommentsByPostId(Integer id);

    void postReply(CommentDto commentDto, String content);

    CommentDto getCommentByCommentId(Integer comment_id);

    void postComment(String content, Integer post_id);

    void updateComment(Integer comment_id, String content);

    void deleteComment(Integer comment_id, Integer parent_id);
}
