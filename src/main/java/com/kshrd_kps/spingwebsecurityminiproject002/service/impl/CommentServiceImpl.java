package com.kshrd_kps.spingwebsecurityminiproject002.service.impl;

import com.kshrd_kps.spingwebsecurityminiproject002.model.CommentDto;
import com.kshrd_kps.spingwebsecurityminiproject002.repository.CommentRepository;
import com.kshrd_kps.spingwebsecurityminiproject002.service.CommentService;
import com.kshrd_kps.spingwebsecurityminiproject002.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final UserService userService;

    public CommentServiceImpl(CommentRepository commentRepository, UserService userService) {
        this.commentRepository = commentRepository;
        this.userService = userService;
    }

    @Override
    public CommentDto getCommentsByPostId(Integer id) {
        return commentRepository.getCommentsByPostId(id);
    }

    @Override
     public void postReply(CommentDto commentDto, String content) {
        commentRepository.postReply(commentDto, content);
    }

    @Override
    public CommentDto getCommentByCommentId(Integer comment_id) {
        return commentRepository.getCommentByCommentId(comment_id);
    }

    @Override
    public void postComment(String content, Integer post_id) {
        String username = userService.getUserNameFromToken();
        Integer userId = userService.getUserIdFromName(username);
        commentRepository.postComment(content, post_id, userId);
    }

    @Override
    public void updateComment(Integer comment_id, String content) {
        commentRepository.updateComment(comment_id, content);
    }

    @Override
    public void deleteComment(Integer comment_id, Integer parent_id) {
        commentRepository.deleteComment(comment_id, parent_id);
    }
}
