package com.kshrd_kps.spingwebsecurityminiproject002.service.impl;

import com.kshrd_kps.spingwebsecurityminiproject002.model.PostDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.PostRequest;
import com.kshrd_kps.spingwebsecurityminiproject002.repository.PostRepository;
import com.kshrd_kps.spingwebsecurityminiproject002.service.PostService;
import com.kshrd_kps.spingwebsecurityminiproject002.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final UserService userService;

    public PostServiceImpl(PostRepository postRepository, UserService userService) {
        this.postRepository = postRepository;
        this.userService = userService;
    }

    @Override
    public List<PostDto> getAllPosts(Integer page, Integer size) {
        return postRepository.getAllPosts(page, size);
    }

    @Override
    public PostDto getPostByPostId(Integer post_id) {

        return postRepository.getPostByPostId(post_id);
    }

    @Override
    public void createPost(PostRequest postRequest) {
        String username = userService.getUserNameFromToken();
        Integer userId = userService.getUserIdFromName(username);
        postRepository.createPost(postRequest, userId);
    }

    @Override
    public PostDto getNewInsertedPost() {
        return postRepository.getNewInsertedPost();
    }

    @Override
    public void updatePost(Integer id, PostRequest postRequest) {
        postRepository.updatePost(id, postRequest);
    }

    @Override
    public void deletePost(Integer id) {
        postRepository.deletePost(id);
    }


}
