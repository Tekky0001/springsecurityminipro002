package com.kshrd_kps.spingwebsecurityminiproject002.service.impl;

import com.kshrd_kps.spingwebsecurityminiproject002.model.User;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserRequest;
import com.kshrd_kps.spingwebsecurityminiproject002.repository.UserRepository;
import com.kshrd_kps.spingwebsecurityminiproject002.repository.UserRoleRepository;
import com.kshrd_kps.spingwebsecurityminiproject002.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final PasswordEncoder passwordEncoder;
    public UserServiceImpl(UserRepository userRepository, UserRoleRepository userRoleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void signUpUser(UserDto user) {
        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPassword);
        userRepository.insertUser(user);
        User aUser = userRepository.getUserByUsername(user.getUsername());
        for(int i=0; i<user.getRoles().size(); i++){
            Integer roleId = 0;
            if(Objects.equals(user.getRoles().get(i), "ADMIN")){
                roleId = 1;
            }
            else {
                roleId = 2;
            }
//            System.out.println("****************************");
//            System.out.println(" Here is the role Id : "+ roleId);
           userRoleRepository.insertUserRole(aUser.getId(),roleId);
        }



    }

    @Override
    public User matchUser(UserRequest userRequest) {
        return userRepository.matchuser(userRequest);
    }

    @Override
    public User getLatestUser(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public String getUserNameFromToken() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        String username = userDetails.getUsername();
        return username;
    }

    @Override
    public Integer getUserIdFromName(String username) {
        return userRepository.getUserByUsername(username).getId();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.getUserByUsername(username);
    }
}
