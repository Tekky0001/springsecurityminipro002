package com.kshrd_kps.spingwebsecurityminiproject002.service;

import com.kshrd_kps.spingwebsecurityminiproject002.model.User;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserRequest;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    void signUpUser(UserDto user);

    User matchUser(UserRequest userRequest);

    User getLatestUser(String username);
    String getUserNameFromToken();

    Integer getUserIdFromName(String username);
}
