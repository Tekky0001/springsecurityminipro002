package com.kshrd_kps.spingwebsecurityminiproject002.controller;

import com.kshrd_kps.spingwebsecurityminiproject002.model.Response;
import com.kshrd_kps.spingwebsecurityminiproject002.model.User;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserRequest;
import com.kshrd_kps.spingwebsecurityminiproject002.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/api/v1/auth")
public class AuthController {
//    @GetMapping("/login")
//    public ModelAndView loginPage(){
//        ModelAndView mv = new ModelAndView();
//        mv.setViewName("login");
//        System.out.println("hello");
//        return mv;
//    }
//    @GetMapping("/signup")
//    public ModelAndView signUpPage(){
//        ModelAndView mv = new ModelAndView();
//        mv.setViewName("signup");
//        return mv;
//    }
    private final UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/basic/login")
    public ResponseEntity<?> login(@RequestBody UserRequest userRequest) {
        User user = userService.matchUser(userRequest);
        Response<User> response = Response
                .<User>builder()
                .error("Successfully fetched all the articles")
                .metadata(null)
                .payload(user)
                .status("OK")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }
    @PostMapping("/basic/signup")
    public ResponseEntity<?> signUp(@RequestBody UserDto userRequest){
        System.out.println(userRequest);
        userService.signUpUser(userRequest);
        User user = userService.getLatestUser(userRequest.getUsername());
        Response<User> response = Response
                .<User>builder()
                .error("Successfully fetched all the articles")
                .metadata(null)
                .payload(user)
                .status("OK")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

}
