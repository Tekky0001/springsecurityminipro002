package com.kshrd_kps.spingwebsecurityminiproject002.controller;

import com.kshrd_kps.spingwebsecurityminiproject002.model.Comment;
import com.kshrd_kps.spingwebsecurityminiproject002.model.CommentDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.PostDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.Response;
import com.kshrd_kps.spingwebsecurityminiproject002.service.CommentService;
import com.kshrd_kps.spingwebsecurityminiproject002.service.PostService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/posts")
@SecurityRequirement(name = "bearerAuth")
public class CommentController {
    private final CommentService commentService;
    private final PostService postService;
    public CommentController(CommentService commentService, PostService postService) {
        this.commentService = commentService;
        this.postService = postService;
    }
    @PostMapping("/{comment_id}/reply")
    public ResponseEntity<?> postReply(
            @PathVariable("comment_id") Integer comment_id,
            @RequestParam String content
    ){
        CommentDto commentDto = commentService.getCommentByCommentId(comment_id);
        System.out.println(commentDto);
        commentService.postReply(commentDto, content);
        PostDto postDto = postService.getPostByPostId(commentDto.getPost_id());
        Response<PostDto> response = Response
                .<PostDto>builder()
                .payload(postDto)
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();
        return ResponseEntity.ok(response);
    }

    @PostMapping("/{post_id}/comment")
    public ResponseEntity<?> postComment(
            @PathVariable("post_id") Integer post_id,
            @RequestParam String content
    ) {
        commentService.postComment(content, post_id);
        PostDto postDto = postService.getPostByPostId(post_id);
        Response<PostDto> response = Response
                .<PostDto>builder()
                .payload(postDto)
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();
        return ResponseEntity.ok(response);
    }
    @PatchMapping("/{comment_id}/update-comment")
    public ResponseEntity<?> updateComment(
            @PathVariable("comment_id") Integer comment_id,
            @RequestParam String content
    ){
        commentService.updateComment(comment_id, content);
        CommentDto comment = commentService.getCommentByCommentId(comment_id);
        PostDto postDto = postService.getPostByPostId(comment.getPost_id());
        Response<PostDto> response = Response
                .<PostDto>builder()
                .payload(postDto)
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/{comment_id}/delete-comment")
    public ResponseEntity<?> deleteComment(
            @PathVariable("comment_id") Integer comment_id,
            @RequestParam Integer parent_id
    ){
        CommentDto commentDto = commentService.getCommentByCommentId(comment_id);
        commentService.deleteComment(comment_id, parent_id);
        PostDto postDto = postService.getPostByPostId(commentDto.getPost_id());
        Response<PostDto> response = Response
                .<PostDto>builder()
                .payload(postDto)
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();
        return ResponseEntity.ok(response);
    }
}
