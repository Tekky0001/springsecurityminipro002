package com.kshrd_kps.spingwebsecurityminiproject002.controller;

import com.kshrd_kps.spingwebsecurityminiproject002.model.*;
import com.kshrd_kps.spingwebsecurityminiproject002.service.CommentService;
import com.kshrd_kps.spingwebsecurityminiproject002.service.PostService;
import com.kshrd_kps.spingwebsecurityminiproject002.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/posts")
@SecurityRequirement(name = "bearerAuth")
public class PostController {
    private final PostService postService;
    private final CommentService commentService;
    private final UserService userService;

    public PostController(PostService postService, CommentService commentService, UserService userService) {
        this.postService = postService;
        this.commentService = commentService;
        this.userService = userService;
    }

    @GetMapping("/find-all")
    public ResponseEntity<?> getAllPosts(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "2") Integer size
    ){
        List<PostDto> posts = postService.getAllPosts(page, size);
        Response<List<PostDto>> response = Response
                .<List<PostDto>>builder()
                .payload(posts)
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();

        return ResponseEntity.ok(response);
    }
    @PostMapping("/create-post")
    public ResponseEntity<?> createPost(@RequestBody PostRequest postRequest){


        postService.createPost(postRequest);
        PostDto postDto = postService.getNewInsertedPost();
        Response<PostDto> response = Response
                .<PostDto>builder()
                .payload(postDto)
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}/view")
    public ResponseEntity<?> viewPost(@PathVariable("id") Integer id){
        PostDto postDto =  postService.getPostByPostId(id);
        Response<PostDto> response = Response
                .<PostDto>builder()
                .payload(postDto)
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();
        return ResponseEntity.ok(response);
    }
    @PatchMapping("/{id}/update-post")
    public ResponseEntity<?> updatePost(
            @PathVariable("id") Integer id,
            @RequestBody PostRequest postRequest
    ){
        postService.updatePost(id, postRequest);
        PostDto postDto =  postService.getPostByPostId(id);
        Response<PostDto> response = Response
                .<PostDto>builder()
                .payload(postDto)
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/{id}/delete-post")
    public ResponseEntity<?>deletePost(@PathVariable("id") Integer id){
        postService.deletePost(id);
        Response<String> response = Response
                .<String>builder()
                .payload("")
                .error("Successfully fetched all the articles")
                .success(true)
                .metadata(null)
                .status("OK")
                .build();
        return ResponseEntity.ok(response);
    }

}
