package com.kshrd_kps.spingwebsecurityminiproject002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private Integer id;
    private String caption;
    private String image;
    private Integer number_of_like;
    private Integer user_id;

}
