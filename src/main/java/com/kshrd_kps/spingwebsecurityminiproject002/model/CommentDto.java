package com.kshrd_kps.spingwebsecurityminiproject002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDto {
    private String content;
    private Integer id;
    private Integer parent_id;
    private Integer post_id;
    private List<CommentDto> replies;
    private Integer user_id;
}
