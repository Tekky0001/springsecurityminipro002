package com.kshrd_kps.spingwebsecurityminiproject002.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Response<T> {
    private T payload;
    private String error;
    private boolean success;
    private String metadata;
    private String status;
}
