package com.kshrd_kps.spingwebsecurityminiproject002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {

    private String caption;
    private List<CommentDto> comments;
    private Integer id;
    private String image;
    private Integer number_of_like;
    private boolean owner;
    private Integer user_id;
    private String username;
}
