package com.kshrd_kps.spingwebsecurityminiproject002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private String full_name;
    private String image_url;
    private String password;
    private String username;
    private List<String> roles;
}