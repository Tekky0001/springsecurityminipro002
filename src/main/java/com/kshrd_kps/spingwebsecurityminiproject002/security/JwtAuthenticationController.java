package com.kshrd_kps.spingwebsecurityminiproject002.security;

import com.kshrd_kps.spingwebsecurityminiproject002.model.Response;
import com.kshrd_kps.spingwebsecurityminiproject002.model.User;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserDto;
import com.kshrd_kps.spingwebsecurityminiproject002.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenUtil jwtTokenUtil;

    private final UserService userDetailsService;

    public JwtAuthenticationController(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, UserService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping(value = "/api/v1/auth/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));
    }
    @PostMapping("/api/v1/auth/signup")
    public ResponseEntity<?> signUp(@RequestBody UserDto userRequest){
        //System.out.println(userRequest);
        userDetailsService.signUpUser(userRequest);
        User user = userDetailsService.getLatestUser(userRequest.getUsername());
        Response<User> response = Response
                .<User>builder()
                .error("Successfully fetched all the articles")
                .metadata(null)
                .payload(user)
                .status("OK")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}