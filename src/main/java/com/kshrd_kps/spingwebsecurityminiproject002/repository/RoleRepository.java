package com.kshrd_kps.spingwebsecurityminiproject002.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RoleRepository {
    @Select("SELECT r.role_status " +
            "FROM user_role ur " +
            "INNER JOIN roles r ON r.id = ur.role_id " +
            "WHERE ur.user_id = #{user_id};")
    List<String>getUserRoles(@Param("user_id") Integer userId);
}
