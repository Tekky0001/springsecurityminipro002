package com.kshrd_kps.spingwebsecurityminiproject002.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserRoleRepository {

    @Insert("insert into user_role (user_id, role_id) values (#{id}, #{roleId})")
    void insertUserRole(Integer id, Integer roleId);
}
