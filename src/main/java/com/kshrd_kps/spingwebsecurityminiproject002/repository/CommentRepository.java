package com.kshrd_kps.spingwebsecurityminiproject002.repository;

import com.kshrd_kps.spingwebsecurityminiproject002.model.Comment;
import com.kshrd_kps.spingwebsecurityminiproject002.model.CommentDto;
import org.apache.ibatis.annotations.*;


@Mapper
public interface  CommentRepository {
    @Select("select * from comments where post_id = #{id} and parent_id = 0")
    @Result(property="id", column ="id")
    @Result(property = "replies" , column = "id",
            many = @Many(
                    select = "com.kshrd_kps.spingwebsecurityminiproject002.repository.CommentRepository.getCommentsByParentId"
            ))
    CommentDto getCommentsByPostId(Integer id);

    @Select("select * from comments where parent_id = #{id}")
    @Result(property="id", column ="id")
    @Result(property = "replies" , column = "id",
            many = @Many(
                    select = "com.kshrd_kps.spingwebsecurityminiproject002.repository.CommentRepository.getCommentsByParentId"
            ))
    CommentDto getCommentsByParentId(Integer id);


    @Insert("insert into comments (content, parent_id, post_id, user_id) values (#{content}, #{comment.id}, #{comment.post_id}, #{comment.user_id})")
    @Result(property="content", column ="content")
    @Result(property="parent_id", column ="parent_id")
    @Result(property="post_id", column ="post_id")
    @Result(property="user_id", column ="user_id")
    void postReply(@Param("comment")CommentDto commentDto, String content);

    @Select("select * from comments where id = #{comment_id}")
    @Result(property="id", column ="id")
    @Result(property = "replies" , column = "parent_id",
            many = @Many(
                    select = "com.kshrd_kps.spingwebsecurityminiproject002.repository.CommentRepository.getCommentsByParentId"
            ))
    CommentDto getCommentByCommentId(Integer comment_id);

    @Insert("insert into comments (content, parent_id, post_id, user_id ) values (#{content}, 0, #{post_id}, #{userId})")
    @Result(property="id", column ="id")

    void postComment(String content, Integer post_id, Integer userId);

    @Update("update comments set content = #{content} where id = #{comment_id}")
    void updateComment(Integer comment_id, String content);

    @Delete("delete from comments where id = #{comment_id}")
    void deleteComment(@Param("comment_id")Integer comment_id, Integer parent_id);
}
