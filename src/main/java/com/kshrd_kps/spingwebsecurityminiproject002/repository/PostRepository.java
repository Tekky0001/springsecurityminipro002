package com.kshrd_kps.spingwebsecurityminiproject002.repository;

import com.kshrd_kps.spingwebsecurityminiproject002.model.Post;
import com.kshrd_kps.spingwebsecurityminiproject002.model.PostDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.PostRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PostRepository {
    @Select("select * from posts limit #{size} offset #{size}*(#{page}-1)")
    @Results(value={
            @Result(property = "id", column = "id"),
            @Result(property="comments", column="id" ,
            many = @Many(select="com.kshrd_kps.spingwebsecurityminiproject002.repository.CommentRepository.getCommentsByPostId"))
    })
    List<PostDto> getAllPosts(Integer page, Integer size);

    @Select("select * from posts where id=#{post_id}")
    @Result(property = "id", column = "id")
    @Results(value={
            @Result(property="comments", column="id" ,
                    many = @Many(select="com.kshrd_kps.spingwebsecurityminiproject002.repository.CommentRepository.getCommentsByPostId"))
    })

    PostDto getPostByPostId(Integer post_id);

    @Insert("insert into posts (caption, image, number_of_like, user_id) values (#{post.caption}, #{post.image}, 0, #{userId})")
    @Result(property = "caption", column = "caption")
    @Result(property = "image", column = "image")
    void createPost(@Param("post") PostRequest postRequest, Integer userId);

    @Select("select * from posts order by id desc limit 1")
    @Result(property = "id", column = "id")
    PostDto getNewInsertedPost();

    @Update("update posts set caption = #{post.caption}, image = #{post.image} where id=#{id}")
    void updatePost(Integer id, @Param("post") PostRequest postRequest);

    @Delete("delete from posts where id = #{id}")
    void deletePost(Integer id);
}
