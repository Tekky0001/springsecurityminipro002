package com.kshrd_kps.spingwebsecurityminiproject002.repository;

import com.kshrd_kps.spingwebsecurityminiproject002.model.User;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserDto;
import com.kshrd_kps.spingwebsecurityminiproject002.model.UserRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.security.core.userdetails.UserDetails;

@Mapper
public interface UserRepository {
    @Select("select * from users where username = #{request.username} and password = #{request.password}")
    @Result(property = "id", column = "id")
    @Result(property = "full_name", column = "full_name")
    @Result(property = "roles", column = "id",
            many = @Many(select="com.kshrd_kps.spingwebsecurityminiproject002.repository.RoleRepository.getUserRoles"))
    User matchuser(@Param("request") UserRequest user);
    @Insert("insert into users (username, password, full_name, image_url) values (#{user.username}, #{user.password}, #{user.full_name}, #{user.image_url})")
    void insertUser(@Param("user") UserDto user);

    @Select("select * from users where username = #{username}")
    @Result(property = "id", column = "id")
    @Result(property = "full_name", column = "full_name")
    @Result(property = "roles", column = "id",
            many = @Many(select="com.kshrd_kps.spingwebsecurityminiproject002.repository.RoleRepository.getUserRoles"))
    User getUserByUsername(String username);
}
